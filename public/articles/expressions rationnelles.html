<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Articles et tutoriels de Benjamin Galliot – Focus (CNRS UAR2259 Ardis)</title>
    <link rel="stylesheet" href="../style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Gentium+Plus&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Gentium+Plus&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+TC&display=swap" rel="stylesheet">
</head>
<body>
  <h1 id="les-expressions-rationnelles">Les expressions rationnelles (<em>regex</em>)</h1>
  <h2 id="introduction">Introduction</h2>
  <p>Aujourd’hui, nous allons vous présenter les <strong>expressions rationnelles</strong>, aussi appelées <strong>expressions régulières</strong> ou tout simplement <strong><em>regex</em></strong> (contraction de <em><strong>reg</strong>ular <strong>ex</strong>pression</em>). Qu’est-ce donc ? C’est un outil puissant, mais pas toujours intuitif, notamment du fait d’une syntaxe assez abstruse, pour manipuler un texte, quel qu’il soit.</p>
  <p>Imaginez qu’après avoir bouclé votre thèse, tout près de l’échéance, vous vous apercevez que toutes vos dates au format américain doivent être mises au format français, ou que les caractères chinois doivent être mis dans un style calligraphique, loin de la police par défaut, et cela sur 500 pages… Et si vous ne passez pas vos journées sur de la rédaction d’articles, imaginez qu’après de longues recherches, vous retrouvez des milliers de lignes de données textuelles dans un vieux format douteux, mais que vous devez les remettre d’aplomb pour les valoriser auprès de vos confrères dans un format interopérable…</p>
  <p>Utiliser une expression rationnelle vous permettra de faire ces modifications très rapidement et de manière bien plus sûre que le simple <em>rechercher/remplacer</em> habituel de votre éditeur de texte favori.</p>
  <p>Les développeurs informatiques connaissent généralement suffisamment bien cet outil, mais il gagnerait à être plus connu en dehors de ce domaine, puisqu’il s’agit d’un outil très utile aussi en édition, et en fait dans tous les domaines où l’on doit manipuler du texte (vérification, recherche ou modification).</p>
  <p>Ce court article est là pour présenter les possibilités qu’offrent les expressions rationnelles, il ne s’agit pas d’un tutoriel (la Toile en regorge…), mais de quelques illustrations variées pour tenter de démontrer leur utilité.</p>
  <p>Tout d’abord, si vous écrivez vos textes avec un logiciel (traitement de texte, publication assistée par ordinateur, etc.), il est possible que certaines modifications soient automatisées ou préconfigurées pour un appel ponctuel rapide. Dans tous les cas, certains logiciels permettent d’avoir accès à ces outils (parfois sous d’autres noms, comme <em>grep</em> sur InDesign). Il est aussi possible d’installer des extensions pour cela sur vos navigateurs, si vous avez besoin de faire des recherches plus fines sur la Toile.</p>
  <h2 id="contexte">Contexte</h2>
  <p>Dès lors que vous devez faire une recherche ou une modification d’une expression non littérale (comme pourrait l’être un caractère précis), mais sur une expression composée d’éléments abstraits ou variables, possédant une certaine logique, nous allons devoir utiliser un <strong>modèle</strong> (ou <strong>motif</strong>, une formule décrivant l’expression rationnelle ; en anglais <strong><em>pattern</em></strong>). Ce modèle permettra d’utiliser des caractères particuliers pour désigner par exemple des ensembles (tous les chiffres, toutes les lettres, etc.), leur nombre d’occurrences, leur structure, les éléments nécessaires, facultatifs, etc.</p>
  <p>Plus généralement, dès que vous avez besoin de trouver un modèle de texte précis, ou de modifier un modèle par un autre, plusieurs choix s’offrent à vous :</p>
  <ul>
    <li>Vous pouvez tout faire « à la main », mais c’est très chronophage et le taux d’erreur est rarement satisfaisant, mais pour un texte très court, c’est parfois très bien !</li>
    <li>Vous pouvez demander à votre logiciel de gérer cela pour vous (selon votre maîtrise de ce dernier et ses propres possibilités), c’est généralement très bien, mais limité par ce que le logiciel propose déjà, donc plus le cas sera complexe, moins ce sera possible !</li>
    <li>Vous pouvez demander à votre collègue <em>informaticien</em> (terme que les <em>informaticiens</em> utilisent rarement entre eux…) de faire cela pour vous, mais vous n’en aurez pas toujours un de disponible.</li>
    <li>Vous pouvez apprendre quelques bases des expressions rationnelles pour accroître votre indépendance technique et vous ouvrir tout un nouvel univers de possibilités textuelles (en plus d’éprouver un certain sentiment de fierté) !</li>
  </ul>
  <h2 id="illustrations">Illustrations</h2>
  <h3 id="conversion-du-séparateur-décimal">Conversion du séparateur décimal</h3>
  <p>Vous désirez convertir des nombres décimaux en modifiant le séparateur (virgule en français, point en anglais, notamment).</p>
  <ul>
    <li>
      Rechercher simplement les <em>nombres à point</em> :
      <ul>
        <li>rechercher : <code>\d+\.\d+</code>
          <br/>où <code>\d</code> signifie l’ensemble <em>n’importe quel chiffre</em>, <code>+</code> est le quantificateur <em>au moins 1 fois</em> et <code>\.</code> le caractère <em>point</em> (<code>.</code> signifie quant à lui <em>n’importe quel caractère</em>).
        </li>
      </ul>
    </li>
    <li>
      Rechercher les <em>nombres à point ou à virgule</em> :
      <ul>
        <li>rechercher : <code>\d+(\.|,)\d+</code>
          <br/>où <code>|</code> est la coordination <em>ou</em> (alternative), les différents choix étant entre parenthèses <code>(…)</code> pour bien structurer et préciser les limites de l’alternative (en plus d’en faire un groupe…).
        </li>
      </ul>
    </li>
    <li>
      Remplacer les <em>nombres à point</em> par des <em>nombres à virgule</em> :
      <ul>
        <li>rechercher : <code>(\d+)\.(\d+)</code>
          <br/>où les deux blocs précédents, de part et d’autre du point, sont mis entre parenthèses, pour en faire des <strong>groupes de capture</strong> (ici numérotés automatiquement) auxquels on pourra se référer dans le remplacement ;
        </li>
        <li>remplacer : <code>$1,$2</code>
          <br/>où <code>$n</code> se réfère au groupe de capture n, donc ici les groupes 1 et 2 de la recherche associée, et <code>,</code> est la virgule qui se substitue donc au point.
        </li>
      </ul>
    </li>
  </ul>
  <h3 id="unifier-des-espaces">Unifier des espaces</h3>
  <p>Vous désirez unifier les espaces, c’est-à-dire ramener à l’unité ces espaces successives (terme féminin en typographie) parfois hétérogènes (espaces simples, insécables, tabulation…).</p>
  <ul>
    <li>rechercher : <code>\s+</code>
      <br/>où <code>\s</code> représente l’ensemble <em>n’importe quelle espace</em> – simple, insécable, fine, etc. ;
    </li>
    <li>remplacer : <code>&#8239;</code>
      <br/>où il se trouve une seule espace.
    </li>
  </ul>
  <h3 id="ajout-d-espace-insécable-devant-des-ponctuations-doubles">Ajout d’espace insécable devant des ponctuations doubles</h3>
  <p>Vous désirez ajouter en français une espace fine insécable devant certaines ponctuations doubles (l’orthotypographie est un peu plus complexe, mais c’est un exemple).</p>
  <ul>
    <li>rechercher : <code>\s*([?!;:])</code>
      <br/>où ce qui se trouve entre les crochets <code>[…]</code> est une classe de caractères, ici composée de quatre ponctuations doubles, classe elle-même incluse dans un groupe, précédé de n’importe quelle espace (<code>\s</code>), existant ou non (quantificateur <code>*</code>) ;
    </li>
    <li>remplacer : <code>&#8239;$1</code>
      <br/>où l’espace fine insécable placée avant le rappel au groupe de capture 1 est important.
    </li>
  </ul>
  <h3 id="convertir-des-formats-de-date">Convertir des formats de date</h3>
  <p>Vous désirez convertir des dates à l’américaine (mois/jour/année) en dates à la française (jour/mois/année).</p>
  <ul>
    <li>rechercher : <code>(?&lt;mois&gt;\d+)/(?&lt;jour&gt;\d+)/(?&lt;année&gt;\d+)</code>
      <br/>où nous avons trois groupes de captures (ici nommés grâce à <code>?&lt;nom&gt;</code> à l’ouverture du groupe) formés de nombres décimaux séparés par le caractère <code>/</code> ;
    </li>
    <li>remplacer : <code>${jour}/${mois}/${année}</code>
      <br/>où nous intervertissons les deux premiers groupes des jours et des mois (on se réfère à présent au groupe nommé à nouveau par <code>$</code>, mais suivi du nom – entre accolades <code>{…}</code> – et non plus du numéro implicite).
    </li>
  </ul>
  <h3 id="rechercher-des-scripts-particuliers">Rechercher des scripts particuliers</h3>
  <p>Vous désirez trouver des caractères plus particuliers, notamment dans un texte multilingue.</p>
  <ul>
    <li>
      Rechercher tous les caractères chinois (<em>lato sensu</em> : chinois, japonais, coréen), et les insérer entre des balises pour leur appliquer un style particulier, par exemple pour transformer le texte suivant :
      <p><code>le conte 娇娜 (嬌娜) est tiré de l’œuvre 聊斋志异 (聊齋誌異)</code></p>
      <p>en un texte mieux formaté :</p>
      <p><code>le conte &lt;span class=&quot;chinois&quot;&gt;娇娜&lt;/span&gt; (&lt;span class=&quot;chinois&quot;&gt;嬌娜&lt;/span&gt;) est tiré de l’œuvre &lt;span class=&quot;chinois&quot;&gt;聊斋志异&lt;/span&gt; (&lt;span class=&quot;chinois&quot;&gt;聊齋誌異&lt;/span&gt;)</code></p>
      <p>Ce qui pourrait donner (selon les polices de caractères à votre disposition…) :</p>
      <p>« le conte <span class="chinois">娇娜</span> (<span class="chinois">嬌娜</span>) est tiré de l’œuvre <span class="chinois">聊斋志异</span> (<span class="chinois">聊齋誌異</span>) »</p>
      <ul>
        <li>rechercher : <code>\p{Script=Han}+</code>
          <br/>où <code>\p{Script=Han}</code> est une propriété Unicode précisant l’ensemble des caractères appartenant au script han (« chinois »).
        </li>
        <li>remplacer : <code>&lt;span class=&quot;chinois&quot;&gt;$0&lt;/span&gt;</code>
          <br/> où le groupe de capture 0 correspond à la totalité de l’expression.
        </li>
      </ul>
    </li>
    <li>
      Rechercher tous les caractères combinants pas toujours faciles à détecter (diacritiques modifiant d’autres caractères, etc.) :
      <ul>
        <li>rechercher : <code>\p{M}</code>
          <br/>où <code>\p{…}</code> est une <strong>propriété Unicode</strong>, ici précisant l’ensemble des caractères combinants.
        </li>
      </ul>
    </li>
  </ul>
  <h3 id="formater-un-segment-de-texte-en-xml">Formater un segment de texte en XML</h3>
  <p>Si vous désirez formater un segment de texte qui suit une certaine logique (plus ou moins variable) au format XML, par exemple transformer le segment suivant (comportant quelques  caractères combinants) :</p>
  <pre><code style="font-family: 'Gentium Plus', serif;">fra     endurer     /ɑ̃.dy.ʁe/     [ɒ̃d̪yˈʁe]</code></pre>
  <p>en un bloc XML de cette forme :</p>
  <code>&lt;mot langue="fra"&gt;&#10;<br/>
    &#8239&#8239&lt;orthographe script="lat"&gt;endurer&lt;/orthographe&gt;<br/>
    &#8239&#8239&lt;phonologie script="api"&gt;ɑ̃.dy.ʁe&lt;/phonologie&gt;<br/>
    &#8239&#8239&lt;phonétique script="api"&gt;ɒ̃d̪yˈʁe&lt;/phonétique&gt;<br/>
  &lt;/mot&gt;</code>
  <ul>
    <li>rechercher : <code>(?&lt;langue&gt;\w+)\s+(?&lt;orthographe&gt;\w+)\s+/(?&lt;phonologie&gt;[\p{L}\p{M}.]+)/\s+\[(?&lt;phonétique&gt;[\p{L}\p{M}]+)\]</code>
      <br/>où <code>\w</code> représente l’ensemble <em>n’importe quelle lettre</em>, <code>\p{L}</code> la propriété Unicode <em>n’importe quelle lettre</em> (ils sont un peu différents…), et les crochets littéraux (<code>[</code> et <code>]</code>) sont là aussi <em>protégés</em> par <code>\</code> ;
    </li>
    <li>remplacer : <code>&lt;mot langue=&quot;$langue&quot;&gt;\n&#8239;&#8239;&lt;orthographe script=&quot;lat&quot;&gt;$orthographe&lt;/orthographe&gt;\n&#8239;&#8239;&lt;phonologie script=&quot;api&quot;&gt;$phonologie&lt;/phonologie&gt;\n&#8239;&#8239;&lt;phonétique script=&quot;api&quot;&gt;$phonétique&lt;/phonétique&gt;\n&lt;/mot&gt;</code>
      <br/>où <code>\n</code> est le <em>saut de ligne</em>.
    </li>
  </ul>
  <h2 id="quelques-explications">Quelques explications</h2>
  <p>L’usage de la barre oblique inversée (<em>antislash</em> en anglais) signifie que le caractère qui suit est à prendre dans un sens particulier… Comme dans ce langage, il existe des caractères <strong>spéciaux</strong>, appelés <strong>opérateurs</strong> (ou <strong>métacaractères</strong>) – par opposition aux caractères <strong>littéraux</strong> –, comme ce <code>+</code> qui n’est pas le caractère « + » mais un <strong>quantificateur</strong> (un type d’opérateur) simple (stipulant que l’<strong>expression</strong> qui précède doit être présent au moins 1 fois), ou bien ce <code>.</code> qui est le caractère joker (n’importe quel caractère), il est nécessaire d’ajouter devant eux ce <code>\</code> pour les distinguer des caractères littéraux. Ainsi, <code>\+</code> signifie littéralement « + », de même que <code>\.</code> signifie littéralement « . ».
    Néanmoins, ce n’est pas si simple, car nous avons aussi vu dans ce petit exemple que <code>\d</code> ne signifie pas littéralement « d ». La règle du <code>\</code> change pour désigner ces <strong>classes de caractères</strong>, et là la lettre qui suit est un moyen mnémotechnique (pour les anglophones…) de savoir à quelle classe de caractères il fait référence.
  </p>
  <p>Présentons quelques opérateurs utiles (pour la plupart vus lors des illustrations) :</p>
  <ul>
    <li><code>|</code> est la conjonction de coordination <em>ou</em> (alternative entre plusieurs options) ;</li>
    <li><code>+</code> permet de quantifier à <em>au moins 1 fois</em> l’expression précédente ;</li>
    <li><code>*</code> permet de quantifier à <em>au moins 0 fois</em> l’expression précédente ;</li>
    <li><code>?</code> permet de quantifier à <em>au plus 1 fois</em> l’expression précédente ;</li>
    <li><code>{</code> et <code>}</code> permettent de préciser davantage les occurrences (<code>x{7,9}</code> permet de stipuler « x » entre 7 et 9 fois, <code>x{7,}</code> au moins 7 fois, <code>x{,9}</code> au plus 9 fois et <code>x{9}</code> exactement 9 fois) ;</li>
    <li><code>(</code> et <code>)</code> permettent de faire des groupes de capture (pour structurer, notamment avec les quantificateurs, et pour s’y référer plus tard…) :
      <ul>
        <li><code>(…)</code> permet de faire des groupes de capture numérotés implicitement et <code>$n</code> de s’y référer ensuite ;</li>
        <li><code>(?&lt;nom&gt;…)</code> permet de faire des groupes de capture nommés explicitement et <code>${nom}</code> de s’y référer ensuite (la syntaxe peut grandement varier…) ;</li>
      </ul>
    </li>
    <li><code>[</code> et <code>]</code> permettent de former des classes de caractères (<code>[xyz]+</code> permet de stipuler « x », « y » ou « z » au moins 1 fois) ;</li>
    <li><code>-</code> permet, dans une classe de caractères formée par <code>[…]</code>, de former une plage entre 2 caractères – adresses Unicode comprises, avec <code>\u</code> (<code>[0-9]</code> correspond à tous les chiffres occidentaux, <code>[\u31c0-\u31e3]</code> correspond aux traits des caractères chinois) ;</li>
    <li><code>^</code> permet, en début d’expression, de dire qu’il s’agit du début de la ligne, ou, dans une classe de caractères formée par <code>[…]</code>, de faire la négation de ce qui suit, et ainsi d’obtenir le complémentaire ;</li>
    <li><code>$</code> permet, en début d’expression, de dire qu’il s’agit de la fin de la ligne ;</li>
    <li><code>.</code> est le <em>joker</em> (n’importe quel caractère) ;</li>
    <li><code>\n</code> est le saut de ligne (<em><strong>n</strong>ewline</em>) (voire parfois <code>\r\n</code> sur Windows) ;</li>
    <li><code>\t</code> est la <strong>t</strong>abulation ;</li>
    <li><code>\w</code> est la classe de caractères des lettres (<em><strong>w</strong>ord</em>) ;</li>
    <li><code>\d</code> est la classe de caractères des chiffres (<em><strong>d</strong>ecimal</em>) ;</li>
    <li><code>\s</code> est la classe de caractères des espaces (<em><strong>s</strong>pace</em>) ;</li>
    <li>la version majuscule de ces classes de caractères est leur propre négation (<code>\W</code> : tout sauf lettre, <code>\D</code> : tout sauf chiffre, <code>\S</code> : tout sauf espace) ;</li>
    <li><code>\p</code> permet de déclarer une propriété Unicode (<em><strong>p</strong>roperties</em>, la syntaxe peut grandement varier…) :
      <ul>
        <li><code>\p{L}</code> est la propriété des lettres (<em><strong>l</strong>etter</em>) :
          <ul>
            <li><code>\p{Ll}</code> est la propriété des lettres minuscules (<em><strong>l</strong>etter <strong>l</strong>owercase</em>) ;</li>
            <li><code>\p{Lu}</code> est la propriété des lettres majuscules (<em><strong>l</strong>etter <strong>u</strong>ppercase</em>) ;</li>
          </ul>
        </li>
        <li><code>\p{N}</code> est la propriété des nombres (<em><strong>n</strong>umber</em>) ;</li>
        <li><code>\p{M}</code> est la propriété des caractères combinants (<em><strong>m</strong>odifier</em>) ;</li>
        <li><code>\p{P}</code> est la propriété des ponctuations (<em><strong>p</strong>unctuation</em>) ;</li>
        <li><code>\p{S}</code> est la propriété des symboles (<em><strong>s</strong>ymbol</em>) ;</li>
        <li><code>\p{Z}</code> est la propriété des espaces…</li>
      </ul>
    </li>
  </ul>
  <h2 id="remarques">Remarques</h2>
  <p>Il existe encore de nombreuses fonctionnalités, comme des conditions variées (« seulement si telle expression existe avant ou après », etc.), des récursions…</p>
  <p>Notons qu’il existe différents moteurs d’expressions rationnelles, avec des différences parfois significatives entre eux (mais il existe quand même une très bonne base commune, rassurez-vous !), et les logiciels incluant cet outil auront généralement choisi un de ces moteurs, sans d’ailleurs forcément y avoir inclus toutes les fonctionnalités de ce dernier (qui peuvent aller très loin…).</p>
  <p>Un bon site que je conseille pour tester ses expressions rationnelles est <a href="http://www.regex101.com">regex101.com</a>. Il permet de tester les expressions, avec différents moteurs, d’avoir des informations colorées, des indications explicites, etc. ! Pour les plus curieux, une bonne référence reste <a href="https://www.regular-expressions.info">regular-expressions.info</a> !</p>
  <h2 id="conclusion">Conclusion</h2>
  <p>Voilà pour cette présentation succincte. Son intérêt était de montrer à des profanes qu’un outil puissant se cache parfois dans leurs logiciels quotidiens, ou qu’il est aisément disponible (site, éditeur de texte…). Je ne m’attends évidemment pas à ce que vous puissiez l’utiliser comme si c’était votre langue maternelle dès la fin de la lecture de ce mini-article, mais si l’existence de ce formidable outil reste dans un coin de votre mémoire et que vous vous en souvenez le jour où il pourra vous être utile, alors cet article aura joué son rôle !</p>
</body>
